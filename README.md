# Flutter Linux Image

This image you can use in your CI/CD pipelines for Android and Linux apps written in Flutter.

Link to image: [registry.gitlab.com/cloudlab_studio/flutter-linux-image:stable](registry.gitlab.com/cloudlab_studio/flutter-linux-image:stable)
It rebuilds every Thursday.

## How to use

Example for Gitlab .gitlab-ci.yml

```yaml
image: registry.gitlab.com/cloudlab_studio/flutter-linux-image:stable

stages:
  - build
  - test

before_script:
  - flutter pub get
  - flutter clean

build:
  stage: build
  script:
    - flutter build linux
  tags:
    - linux

unit_test:
  stage: test
  script:
    - flutter test test/*
  tags:
    - linux
```
